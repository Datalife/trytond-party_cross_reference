datalife_party_cross_reference
==============================

The party_cross_reference module of the Tryton application platform.

[![Build Status](http://drone.datalife.com.es:8050/api/badges/datalife_sco/trytond-party_cross_reference/status.svg)](http://drone.datalife.com.es:8050/datalife_sco/trytond-party_cross_reference)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
